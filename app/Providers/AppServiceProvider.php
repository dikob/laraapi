<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// use below if you want to exclude or don't want to wrapped by "data" object 
// and go directly to the result
//use Illuminate\Http\Resources\Json\JsonResource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // check usage as shown above use Illuminate\Http\Resource\Json\Resource;
        //JsonResource::withoutWrapping();
    }
}
