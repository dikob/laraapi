<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

// App\Article is taken from ArticlesTableSeeder factory setting
$factory->define(App\Article::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->text(50),
        'body'  => $faker->text(200)
    ];
});
